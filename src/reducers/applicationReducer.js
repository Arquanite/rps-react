import {
  SET_GAME_MODE,
  SET_NUMBER_OF_ROUNDS,
  SET_QUICK_PLAY
} from '../actions/application';
import initialState from '../store/initialState';

const applicationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_NUMBER_OF_ROUNDS:
      return {
        ...state,
        numberOfRounds: action.numberOfRounds
      };
    case SET_QUICK_PLAY:
      return {
        ...state,
        quickPlay: action.canQuickPlay
      };
    case SET_GAME_MODE:
      return {
        ...state,
        gameMode: action.gameMode
      };
    default:
      return state;
  }
};

export default applicationReducer;
