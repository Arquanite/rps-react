import { combineReducers } from 'redux';
import gameReducer from './gameReducer';
import applicationReducer from './applicationReducer';

export default combineReducers({
  gameReducer,
  applicationReducer
});