import {
  RESET_GAME,
  NEXT_ROUND,
  SELECT_OPTION,
  SELECT_OPTION_FOR_BOT,
  ADD_POINT_FOR_PLAYER,
  ADD_POINT_FOR_BOT, ADD_POINT_DRAW, SET_HIGHSCORES, SET_HIGHSCORE_DIALOG_VISIBILITY
} from '../actions/game';
import initialState from '../store/initialState';

const gameReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESET_GAME:
      return {
        ...state,
        round: 1,
        playerPoints: 0,
        botPoints: 0,
        draws: 0,
        selectedOption: null,
        selectedOptionForBot: null
      };
    case NEXT_ROUND:
      return {
        ...state,
        round: state.round + 1,
        selectedOption: null,
        selectedOptionForBot: null
      };
    case SELECT_OPTION:
      return {
        ...state,
        selectedOption: action.option
      };
    case SELECT_OPTION_FOR_BOT:
      return {
        ...state,
        selectedOptionForBot: action.option
      };
    case ADD_POINT_FOR_PLAYER:
      return {
        ...state,
        playerPoints: state.playerPoints + 1
      };
    case ADD_POINT_FOR_BOT:
      return {
        ...state,
        botPoints: state.botPoints + 1
      };
    case ADD_POINT_DRAW:
      return {
        ...state,
        draws: state.draws + 1
      };
    case SET_HIGHSCORES:
      return {
        ...state,
        highScores: action.highScores
      };
    case SET_HIGHSCORE_DIALOG_VISIBILITY:
      return {
        ...state,
        canShowHighScoreDialog: action.isVisible
      };
    default:
      return state;
  }
};

export default gameReducer;
