import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { Link } from 'react-router-dom';
import {
  setGameMode,
  setNumberOfRounds,
  setQuickPlay
} from '../../actions/application';
import { connect } from 'react-redux';
import { StyledButton } from '../StyledReactComponents';
import { GAME_MODE } from '../../actions/game';

class Settings extends Component {

  changeNumberOfRounds = e => {
    const { setNumberOfRounds } = this.props;
    setNumberOfRounds(e.target.value);
  };

  changeGameMode = e => {
    const { setGameMode } = this.props;
    setGameMode(e.target.value);
  };

  changeQuickPlay = e => {
    const { setQuickPlay } = this.props;
    setQuickPlay(!e.target.checked);
  };

  render() {
    const { numberOfRounds, quickPlay, gameMode } = this.props;

    const margin = { marginTop: 30 };

    return (
      <div style={{ width: 300 }}>
        <div>Ustawienia</div>
        <FormControl fullWidth style={margin}>
          <InputLabel>Poziom trudności</InputLabel>
          <Select
            value={gameMode}
            onChange={this.changeGameMode}
          >
            <MenuItem value={GAME_MODE.EASY}>Łatwy</MenuItem>
            <MenuItem value={GAME_MODE.MEDIUM}>Średni</MenuItem>
            <MenuItem value={GAME_MODE.HARD}>Trudny</MenuItem>
          </Select>
        </FormControl>
        <FormControl fullWidth style={margin}>
          <InputLabel>Liczba rund</InputLabel>
          <Select
            value={numberOfRounds}
            onChange={this.changeNumberOfRounds}
          >
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={50}>50</MenuItem>
            <MenuItem value={100}>100</MenuItem>
            <MenuItem value={-1}>Nielimitowana</MenuItem>
          </Select>
        </FormControl>
        <FormControlLabel
          style={margin}
          control={<Switch color='primary' onChange={this.changeQuickPlay} checked={!quickPlay} />}
          label="Graj z animacjami"
        />
        <Link to={'/'}>
          <StyledButton style={margin}>
            Powrót
          </StyledButton>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  numberOfRounds: state.applicationReducer.numberOfRounds,
  quickPlay: state.applicationReducer.quickPlay,
  gameMode: state.applicationReducer.gameMode,
});

const mapDispatchToProps = dispatch => ({
  setNumberOfRounds: numberOfRounds => dispatch(setNumberOfRounds(numberOfRounds)),
  setQuickPlay: quickPlay => dispatch(setQuickPlay(quickPlay)),
  setGameMode: gameModel => dispatch(setGameMode(gameModel)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);