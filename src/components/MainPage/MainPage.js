//@flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './MainPage.css';
import { StyledButton } from '../StyledReactComponents';

export default class MainPage extends Component {
  render() {
    return (
      <div>
        <div className='header'>Kamień, papier i nożyce</div>
        <div className='Buttons-Container'>
          <Link to={'/game'}>
            <StyledButton>
              Graj
            </StyledButton>
          </Link>
          <Link to={'/highScores'}>
            <StyledButton>
              Tablica wyników
            </StyledButton>
          </Link>
          <Link to={'/settings'}>
            <StyledButton>
              Ustawienia
            </StyledButton>
          </Link>
          <Link to="/privacy">
            <div style={{ fontSize: 12, marginTop: 50 }}>Polityka prywatności</div>
          </Link>
        </div>
      </div>
    );
  }
}