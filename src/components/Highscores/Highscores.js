import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './Highscores.css';
import { StyledButton } from '../StyledReactComponents';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import { downloadHighscores } from '../../actions/game';

class Highscores extends Component {

  componentDidMount() {
    const { downloadHighScores } = this.props;
    downloadHighScores();
  }

  render() {
    const { highScores } = this.props;

    const sortedHighscores = highScores.sort((a, b) => a.result > b.result ? 1 : a.result < b.result ? -1 : 0).slice(0, 10);

    const margin = { marginTop: 30 };

    return (
      <div style={{ width: 300 }}>
        <div className='HighScores-Header'>Tablica wyników</div>
        <Table size='small'>
          <TableHead>
            <TableRow>
              <TableCell>Pos.</TableCell>
              <TableCell>Nick</TableCell>
              <TableCell>Pkt.</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sortedHighscores.map((highScore, index) => (
              <TableRow aria-keyshortcuts={highScore.name + ' ' + highScore.score}
                        hover
                        key={highScore.id}
                        className='HighScore-Container'>
                <TableCell>{index + 1}.</TableCell>
                <TableCell>{highScore.name}</TableCell>
                <TableCell>{highScore.score}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>

        <Link to={'/'}>
          <StyledButton style={margin}>
            Powrót
          </StyledButton>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  highScores: state.gameReducer.highScores
});

const mapDispatchToProps = dispatch => ({
  downloadHighScores: () => dispatch(downloadHighscores())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Highscores);
