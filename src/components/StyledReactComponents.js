import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core';

const chatWindowStyle = makeStyles({
  root: {
    width: 250,
    border: '1px solid gray'
  }
});

export const StyledButton = ({ style, children, onClick }) => {
  const materialStyle = chatWindowStyle();
  return <Button onClick={onClick} style={style} classes={materialStyle}>{children}</Button>;
};