//@flow
import React, { Component } from 'react';
import { GAME_MODE, GAME_OPTION } from '../../actions/game';
import paper from '../Icons/paper.png';
import scissors from '../Icons/sciccors.png';
import rock from '../Icons/rock.png';

import './Game.css';

type Props = {
  history: any,
  css: string,
  gameMode: string
}

export default class GameHistory extends Component<Props> {

  render() {
    const { history, css, gameMode } = this.props;

    if (gameMode === GAME_MODE.HARD) return null;

    return (
      <div className={css}>
        <div>Historia gry</div>
        <div>
          {history.slice(-5).map(round =>
            <GameHistoryRound
              key={round.id}
              result={round.result}
              playerOption={round.player}
              botOption={round.bot} />
          )}
        </div>
      </div>
    );
  }
}

const GameHistoryRound = ({ result, playerOption, botOption }) => {
  const won = result === 'player';
  const lost = result === 'ai';

  return <div>
    <div className="Flex-Horizontal-Center Game-History-Round">
      <div className={won ? 'Game-History-Win' : null}>
        <GameHistoryIcon option={playerOption} />
      </div>
      <div className={lost ? 'Game-History-Lose' : null}>
        <GameHistoryIcon option={botOption} />
      </div>
    </div>
  </div>;
};

const GameHistoryIcon = ({ option }) => {
  let icon;

  switch (option) {
    case GAME_OPTION.PAPER:
      icon = paper;
      break;
    case GAME_OPTION.SCISSORS:
      icon = scissors;
      break;
    default:
    case GAME_OPTION.ROCK:
      icon = rock;
  }

  return <img src={icon} alt="Icon" />;
};

