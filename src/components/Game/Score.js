//@flow
import React, { Component } from 'react';

import { connect } from 'react-redux';
import '../../App.css';

class Score extends Component {
  render() {
    const { playerPoints, botPoints, draws } = this.props;

    return (
      <div className="Flex-Horizontal-Center Space-Evenly" style={{ marginTop: 20 }}>
        <div>
          <div>Wygrane</div>
          <div>{playerPoints}</div>
        </div>
        <div>
          <div>Remisy</div>
          <div>{draws}</div>
        </div>
        <div>
          <div>Wygrane</div>
          <div>{botPoints}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  playerPoints: state.gameReducer.playerPoints,
  botPoints: state.gameReducer.botPoints,
  draws: state.gameReducer.draws,
});

export default connect(mapStateToProps)(Score);

