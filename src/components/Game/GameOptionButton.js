import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

import paper from '../Icons/paper.png';
import rock from '../Icons/rock.png';
import scissors from '../Icons/sciccors.png';
import { GAME_OPTION } from '../../actions/game';

type Props = {
  disabled: ?boolean,
  onClick: any,
  type: string
}

export default class GameOptionButton extends Component<Props> {
  props: Props;

  render() {
    const { disabled, onClick, type } = this.props;

    return (
      <Button disabled={disabled}
              onClick={onClick}
              className='Option-Button'>
        <GameOptionButtonIcon option={type} />
      </Button>
    );
  }
}

const GameOptionButtonIcon = ({ option }) => {
  switch (option) {
    default:
    case GAME_OPTION.ROCK:
      return <img src={rock} alt='Rock' />;
    case GAME_OPTION.PAPER:
      return <img src={paper} alt='Paper' />;
    case GAME_OPTION.SCISSORS:
      return <img src={scissors} alt='Scissors' />;
  }
};