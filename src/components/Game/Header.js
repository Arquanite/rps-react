//@flow
import React, { Component } from 'react';

import { connect } from 'react-redux';
import '../../App.css';
import { GAME_MODE } from '../../actions/game';

class Header extends Component {
  render() {
    const { round, gameMode, nrOfRounds } = this.props;

    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'flex-start', fontSize: 15 }}>
          <div style={{ marginRight: 5 }}>Poziom trudności:</div>
          <CurrentGameMode gameMode={gameMode} />
        </div>
        <div className="Flex-Horizontal-Center">
          <span style={{ marginRight: 5 }}>Runda: {round}</span>
          <span style={{ marginRight: 5 }}>/</span>
          <span>{nrOfRounds === '-1' ? '∞' : nrOfRounds}</span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  round: state.gameReducer.round,
  gameMode: state.applicationReducer.gameMode,
  nrOfRounds: state.applicationReducer.numberOfRounds
});

export default connect(mapStateToProps)(Header);

const CurrentGameMode = ({ gameMode }) => {
  if (gameMode === GAME_MODE.EASY) {
    return <div>Łatwy</div>;
  } else if (gameMode === GAME_MODE.MEDIUM) {
    return <div>Średni</div>;
  } else if (gameMode === GAME_MODE.HARD) {
    return <div>Trudny</div>;
  }
};