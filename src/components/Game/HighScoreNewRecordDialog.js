import React, { Component } from 'react';
import { connect } from 'react-redux';
import Portal from '@material-ui/core/Portal';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import { resetGame, setHighScoreDialogVisibility } from '../../actions/game';
import { uploadHighScore } from '../../middleware/api-manager';

class HighScoreNewRecordDialog extends Component {

  state = {
    uploadingHighScore: false,
    nick: '',
    filledNick: true
  };

  sendHighScore = async () => {
    const { playerPoints, history } = this.props;
    const { nick } = this.state;

    if (nick == null || nick.length === 0) {
      this.setState({ filledNick: false });
      return;
    }

    this.setState({ uploadingHighScore: true });

    await uploadHighScore(nick, playerPoints);

    setTimeout(() => {
      this.hideDialog();
      this.setState({ uploadingHighScore: false });
      history.push('/highScores');
    }, 1000);
  };

  hideDialog = () => {
    const { hideDialog, resetGame } = this.props;
    hideDialog();
    resetGame();
  };

  onChangeNick = e => {
    this.setState({ nick: e.target.value, filledNick: true });
  };

  render() {
    const { canShowDialog } = this.props;
    const { nick, uploadingHighScore, filledNick } = this.state;

    return (
      <Portal>
        <Dialog open={canShowDialog}>
          <DialogTitle>
            Rekordowy wynik
          </DialogTitle>
          <DialogContent>
            <div> Gratulacje! Zdobyłeś/aś wynik, dzięki któremu trafisz na tablicę rekordów. Podaj swój nick:</div>
            <TextField
              error={!filledNick}
              value={nick}
              required
              onChange={this.onChangeNick}
            />

          </DialogContent>
          <DialogActions>
            <Button onClick={this.hideDialog}>Anuluj</Button>
            <Button onClick={this.sendHighScore}>OK</Button>
          </DialogActions>
          <Loader visible={uploadingHighScore} />
        </Dialog>
      </Portal>
    );
  }
}

const overlapDivStyle = {
  position: 'absolute',
  top: 0,
  display: 'flex',
  width: '100%',
  height: '100%',
  background: '#19191991',
  justifyContent: 'center',
  alignItems: 'center'
};

const Loader = ({ visible }) => {
  if (visible) {
    return <div style={overlapDivStyle}>
      <CircularProgress />
    </div>;
  }
  return null;
};

const mapDispatchToProps = dispatch => ({
  hideDialog: () => dispatch(setHighScoreDialogVisibility(false)),
  resetGame: () => dispatch(resetGame()),
});

const mapStateToProps = state => ({
  canShowDialog: state.gameReducer.canShowHighScoreDialog,
  playerPoints: state.gameReducer.playerPoints
});

export default connect(mapStateToProps, mapDispatchToProps)(HighScoreNewRecordDialog);