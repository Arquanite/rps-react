//@flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import uuid from 'uuid/v1';
import { Link } from 'react-router-dom';
import { StyledButton } from '../StyledReactComponents';
import HighScoreNewRecordDialog from './HighScoreNewRecordDialog';
import { BotHand, PlayerHand } from './GameHandComponents';
import { getResultFromRandom } from '../../middleware/game-manager';
import { sendGameChoiceToAPI } from '../../middleware/api-manager';
import GameHistory from './GameHistory';

import {
  addPointDraw,
  addPointForBot,
  addPointForPlayer,
  nextRound,
  resetGame,
  selectOption,
  selectOptionForBot,
  setHighScoreDialogVisibility,
  GAME_OPTION,
  GAME_MODE,
} from '../../actions/game';
import Header from './Header';
import Score from './Score';

import './Game.css';
import GameOptionButton from './GameOptionButton';

class Game extends Component {
  state = {
    isAnimating: false,
    isWaitingForAnimationFinish: false,
    history: [],
    sessionId: null,
  };

  componentDidMount() {
    const sessionId = uuid();
    this.setState({ sessionId });
  }

  confirmSelectOption = async (userChoice) => {
    const { nextRound, selectOption, quickPlay, maxNumberOfRounds, round, gameMode } = this.props;
    const { history } = this.state;

    this.setState({ isAnimating: true, isWaitingForAnimationFinish: true });

    selectOption(userChoice);
    const roundResult = await this.getRoundResultAsync(gameMode, userChoice);

    setTimeout(() => {
      const botChoice = this.calculateResult_getBotOption(userChoice, roundResult);

      history.push({ id: history.length + 1, player: userChoice, bot: botChoice, result: roundResult });
      this.setState({ isAnimating: false, history });

      setTimeout(() => {
        if (round === parseInt(maxNumberOfRounds, 10)) {
          this.tryAddUserToHighScores();
        } else {
          nextRound();
          this.setState({ isWaitingForAnimationFinish: false });
        }

      }, quickPlay ? 1000 : 1500);
    }, quickPlay ? 0 : 1500);
  };

  getRoundResultAsync = async (gameMode, userChoice) => {
    const { sessionId } = this.state;

    if (gameMode === GAME_MODE.MEDIUM || gameMode === GAME_MODE.HARD) {
      return await sendGameChoiceToAPI(userChoice, sessionId, 2);
    }

    return getResultFromRandom(userChoice);
  };

  calculateResult_getBotOption = (playerOption, roundResult) => {
    const { addPointForPlayer, addPointForBot, addPointDraw, selectOptionForBot } = this.props;
    if (roundResult === 'remis' && playerOption != null) {
      addPointDraw();
      selectOptionForBot(playerOption);
      return playerOption;
    }

    let botOption = GAME_OPTION.PAPER;

    if (roundResult === 'player') {
      addPointForPlayer();
      switch (playerOption) {
        default:
        case GAME_OPTION.ROCK:
          botOption = GAME_OPTION.SCISSORS;
          break;
        case GAME_OPTION.PAPER:
          botOption = GAME_OPTION.ROCK;
          break;
        case GAME_OPTION.SCISSORS:
          botOption = GAME_OPTION.PAPER;
          break;
      }
    } else if (roundResult === 'ai') {
      addPointForBot();
      switch (playerOption) {
        default:
        case GAME_OPTION.ROCK:
          botOption = GAME_OPTION.PAPER;
          break;
        case GAME_OPTION.PAPER:
          botOption = GAME_OPTION.SCISSORS;
          break;
        case GAME_OPTION.SCISSORS:
          botOption = GAME_OPTION.ROCK;
          break;
      }
    }

    selectOptionForBot(botOption);
    return botOption;
  };

  tryAddUserToHighScores() {
    const { history, highScores, playerPoints, showHighScoresDialog } = this.props;

    if (highScores.length >= 10) {
      const min = highScores.reduce((curr, next) => curr < next ? curr : next);
      if (playerPoints > min) {
        showHighScoresDialog();
      } else {
        history.push('/');
      }
    } else {
      showHighScoresDialog();
    }
  }

  backToHome = () => {
    const { resetGame } = this.props;
    resetGame();
  };

  render() {
    const { isAnimating, isWaitingForAnimationFinish, history } = this.state;
    const { selectedOption, selectedOptionForBot, gameMode } = this.props;

    const webPageHistory = this.props.history;

    const buttonsDisabled = isAnimating || isWaitingForAnimationFinish;

    return (
      <div className='Game-Container'>

        <Header />
        <Score />

        <div className='Players-Container'>
          <div className={`Player-Container Player-Container-Player ${isAnimating ? 'Hand-Animation-Player' : null}`}>

            <PlayerHand isAnimating={isAnimating} selectedOption={selectedOption} />

            <ButtonsContainer buttonsDisabled={buttonsDisabled} confirmSelectOption={this.confirmSelectOption} />
          </div>

          <GameHistory css="Game-History" history={history} gameMode={gameMode} />

          <div className={`Player-Container Player-Container-Bot ${isAnimating ? 'Hand-Animation-Bot' : null}`}>
            <BotHand isAnimating={isAnimating} selectedOption={selectedOptionForBot} />
            <div className='Adjust-Background' />
          </div>

        </div>
        <ButtonsContainer buttonsDisabled={buttonsDisabled} type={'Player-Buttons-Small'}
                          confirmSelectOption={this.confirmSelectOption} />

        <GameHistory css="Game-History-Small" history={history} gameMode={gameMode} />

        <div style={{ marginTop: 20 }}>
          <Link to={'/'}>
            <StyledButton onClick={this.backToHome}>
              Powrót
            </StyledButton>
          </Link>
        </div>

        <HighScoreNewRecordDialog history={webPageHistory} />

      </div>
    );
  }
}

const mapStateToProps = state => ({
  round: state.gameReducer.round,
  playerPoints: state.gameReducer.playerPoints,
  botPoints: state.gameReducer.botPoints,
  selectedOption: state.gameReducer.selectedOption,
  selectedOptionForBot: state.gameReducer.selectedOptionForBot,
  maxNumberOfRounds: state.applicationReducer.numberOfRounds,
  quickPlay: state.applicationReducer.quickPlay,
  gameMode: state.applicationReducer.gameMode,
  highScores: state.gameReducer.highScores,
});

const mapDispatchToProps = dispatch => ({
  resetGame: () => dispatch(resetGame()),
  nextRound: () => dispatch(nextRound()),
  selectOption: (option) => dispatch(selectOption(option)),
  selectOptionForBot: (option) => dispatch(selectOptionForBot(option)),
  addPointForPlayer: () => dispatch(addPointForPlayer()),
  addPointForBot: () => dispatch(addPointForBot()),
  addPointDraw: () => dispatch(addPointDraw()),
  showHighScoresDialog: () => dispatch(setHighScoreDialogVisibility(true))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);

const ButtonsContainer = ({ buttonsDisabled, type, confirmSelectOption }) => {

  return <div className={'Player-Buttons ' + type}>
    <GameOptionButton
      onClick={() => confirmSelectOption(GAME_OPTION.ROCK)}
      disabled={buttonsDisabled}
      type={GAME_OPTION.ROCK}
    />
    <GameOptionButton
      onClick={() => confirmSelectOption(GAME_OPTION.PAPER)}
      disabled={buttonsDisabled}
      type={GAME_OPTION.PAPER}
    />
    <GameOptionButton
      onClick={() => confirmSelectOption(GAME_OPTION.SCISSORS)}
      disabled={buttonsDisabled}
      type={GAME_OPTION.SCISSORS}
    />
  </div>;
};
