import React from 'react';
import playerRock from '../Icons/Hands/player-rock.png';
import { GAME_OPTION } from '../../actions/game';
import playerScissors from '../Icons/Hands/player-scissors.png';
import playerPaper from '../Icons/Hands/player-paper.png';
import botRock from '../Icons/Hands/bot-rock.png';
import botScissors from '../Icons/Hands/bot-scissors.png';
import botPaper from '../Icons/Hands/bot-paper.png';

export const PlayerHand = ({ isAnimating, selectedOption }) => {
  let image;

  if (isAnimating) {
    image = playerRock;
  } else {
    switch (selectedOption) {
      case GAME_OPTION.SCISSORS:
        image = playerScissors;
        break;
      case GAME_OPTION.PAPER:
        image = playerPaper;
        break;
      default:
        image = playerRock;
        break;
    }
  }

  return <Hand handImage={image} />;
};

export const BotHand = ({ isAnimating, selectedOption }) => {
  let image;

  if (isAnimating) {
    image = botRock;
  } else {
    switch (selectedOption) {
      case GAME_OPTION.SCISSORS:
        image = botScissors;
        break;
      case GAME_OPTION.PAPER:
        image = botPaper;
        break;
      default:
        image = botRock;
        break;
    }
  }

  return <Hand handImage={image} />;
};

const Hand = ({ handImage }) => {
  return <img src={handImage} alt='Hand' />;
};
