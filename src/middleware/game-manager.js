import { GAME_OPTION } from '../actions/game';

export const getResultFromRandom = (userChoice) => {
  const options = [GAME_OPTION.ROCK, GAME_OPTION.PAPER, GAME_OPTION.SCISSORS];
  const botChoice = options[Math.floor(Math.random() * options.length)];

  if ((userChoice === GAME_OPTION.ROCK && botChoice === GAME_OPTION.SCISSORS)
    || (userChoice === GAME_OPTION.PAPER && botChoice === GAME_OPTION.ROCK)
    || (userChoice === GAME_OPTION.SCISSORS && botChoice === GAME_OPTION.PAPER)) {
    return 'player';
  }

  if ((userChoice === GAME_OPTION.ROCK && botChoice === GAME_OPTION.PAPER)
    || (userChoice === GAME_OPTION.PAPER && botChoice === GAME_OPTION.SCISSORS)
    || (userChoice === GAME_OPTION.SCISSORS && botChoice === GAME_OPTION.ROCK)) {
    return 'ai';
  }

  return 'remis';
};