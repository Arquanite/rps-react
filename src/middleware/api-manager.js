import axios from 'axios';
import { DOWNLOAD_HIGHSCORES, setHighScores } from '../actions/game';
import store from '../store';

export const apiMiddleware = store => next => async action => {
  if (action.type === DOWNLOAD_HIGHSCORES) {
    await downloadHighScoresFromApi();
  }
  next(action);
};

export const uploadHighScore = async (name, score) => {
  await axios.post('https://api.rockpaperscissors.ml/saveHighscore/', {
    name,
    score
  });
};

export const sendGameChoiceToAPI = async (userChoice, sessionId, aiParam) => {
  const response = await axios.get(`https://api.rockpaperscissors.ml/getRound/?user_choice=${userChoice}&session_id=${sessionId}&ai_param=${aiParam}`);
  return response.data;
};

const downloadHighScoresFromApi = async () => {
  const response = await axios.get('https://api.rockpaperscissors.ml/getHighscores/');
  const highScores = response.data;

  const sortedHighScores = highScores.sort((a, b) => a.score > b.score ? -1 : a.score < b.score ? 1 : 0).slice(0, 10);

  store.dispatch(setHighScores(sortedHighScores));
};