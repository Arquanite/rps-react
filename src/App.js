import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core';
import './App.css';
import MainPage from './components/MainPage/MainPage';
import Game from './components/Game/Game';
import Settings from './components/Settings/Settings';
import Highscores from './components/Highscores/Highscores';

import green from '@material-ui/core/colors/green';
import PrivacyPolicy from './components/PrivacyPolicy/PrivacyPolicy';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: green,
  },
});

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path="/game" component={Game} />
            <Route path="/settings" component={Settings} />
            <Route path="/highScores" component={Highscores} />
            <Route path="/privacy" component={PrivacyPolicy} />
            <Route component={MainPage} />
          </Switch>
        </Router>
      </ThemeProvider>
    </div>
  );
}

export default App;
