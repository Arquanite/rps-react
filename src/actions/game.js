//@flow
export const RESET_GAME = 'RESET_GAME';
export const NEXT_ROUND = 'NEXT_ROUND';
export const SELECT_OPTION = 'SELECT_OPTION';
export const SELECT_OPTION_FOR_BOT = 'SELECT_OPTION_FOR_BOT';
export const ADD_POINT_FOR_PLAYER = 'ADD_POINT_FOR_PLAYER';
export const ADD_POINT_FOR_BOT = 'ADD_POINT_FOR_BOT';
export const ADD_POINT_DRAW = 'ADD_POINT_DRAW';
export const DOWNLOAD_HIGHSCORES = 'DOWNLOAD_HIGHSCORES';
export const SET_HIGHSCORES = 'SET_HIGHSCORES';
export const SET_HIGHSCORE_DIALOG_VISIBILITY = 'SET_HIGHSCORE_DIALOG_VISIBILITY';

export const GAME_OPTION = {
  ROCK: 'r',
  PAPER: 'p',
  SCISSORS: 's'
};

export const GAME_MODE = {
  EASY: 'easy',
  MEDIUM: 'medium',
  HARD: 'hard'
};

export function resetGame() {
  return { type: RESET_GAME };
}

export function nextRound() {
  return { type: NEXT_ROUND };
}

export function selectOption(option: string) {
  return { type: SELECT_OPTION, option };
}

export function selectOptionForBot(option: string) {
  return { type: SELECT_OPTION_FOR_BOT, option };
}

export function addPointForPlayer() {
  return { type: ADD_POINT_FOR_PLAYER };
}

export function addPointForBot() {
  return { type: ADD_POINT_FOR_BOT };
}

export function addPointDraw() {
  return { type: ADD_POINT_DRAW };
}

export function downloadHighscores() {
  return { type: DOWNLOAD_HIGHSCORES };
}

export function setHighScores(highScores) {
  return { type: SET_HIGHSCORES, highScores };
}

export function setHighScoreDialogVisibility(isVisible: boolean) {
  return { type: SET_HIGHSCORE_DIALOG_VISIBILITY, isVisible };
}