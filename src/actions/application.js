//@flow
export const SET_NUMBER_OF_ROUNDS = 'SET_NUMBER_OF_ROUNDS';
export const SET_QUICK_PLAY = 'SET_QUICK_PLAY';
export const SET_GAME_MODE = 'SET_GAME_MODE';

export function setNumberOfRounds(numberOfRounds: number) {
  localStorage.setItem('NUMBER_OF_ROUNDS', numberOfRounds);
  return { type: SET_NUMBER_OF_ROUNDS, numberOfRounds };
}

export function setQuickPlay(canQuickPlay: boolean) {
  localStorage.setItem('QUICK_PLAY', canQuickPlay);
  return { type: SET_QUICK_PLAY, canQuickPlay };
}

export function setGameMode(gameMode: string) {
  localStorage.setItem('GAME_MODE', gameMode);
  return { type: SET_GAME_MODE, gameMode };
}