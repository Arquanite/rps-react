import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';
import customMiddleware from './middleware';
import initState from './initialState';

function configureStore(initialState = initState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(...customMiddleware)
  );
}

const store = configureStore();

export default store;