import { GAME_MODE } from '../actions/game';

const initialState = {
  applicationReducer: {
    numberOfRounds: localStorage.getItem('NUMBER_OF_ROUNDS') || 10,
    quickPlay: localStorage.getItem('QUICK_PLAY') || false,
    gameMode: localStorage.getItem('GAME_MODE') || GAME_MODE.EASY
  },
  gameReducer: {
    round: 1,
    playerPoints: 0,
    botPoints: 0,
    draws: 0,
    selectedOption: null,
    selectedOptionForBot: null,
    highScores: [],
    canShowHighScoreDialog: false
  }
};

export default initialState;
