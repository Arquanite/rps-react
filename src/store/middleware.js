import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { apiMiddleware } from '../middleware/api-manager';

const customMiddleware = [thunk, apiMiddleware];

if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    level: 'info',
    collapsed: true
  });
  customMiddleware.push(logger);
}

export default customMiddleware;