const { spawn } = require('child_process');
const fs = require('fs');
const jsdom = require('jsdom');

const buildFacebookApp = async () => {
  const result = await runBuild('yarn.cmd');
  console.log('[FacebookBuilder] Normal build finished with result', result);
  modifyIndexHtml();
};

const runBuild = (yarnCommand) => {
  return new Promise((resolve, reject) => {
    const child = spawn(yarnCommand, ['build']);

    child.stdout.on('data', data => {
      process.stdout.write(data);
    });

    child.stderr.on('data', data => {
      process.stderr.write(data);
    });

    child.on('error', data => {
      console.error(data);
      reject(data);
    });

    child.on('exit', code => {
      if (code === 0) {
        resolve('Build successfull');
      } else {
        reject(`${code}`);
      }
    });
  });
};

const modifyIndexHtml = () => {
  const indexHtml = fs.readFileSync('build/index.html');
  const content = `${indexHtml}`;
  const document = new jsdom.JSDOM(content).window.document;
  const scripts = document.getElementsByTagName('script');

  const windowOnloadScript = document.createElement('script');
  windowOnloadScript.innerHTML = 'window.onload = function () {\n' +
    '        FBInstant.initializeAsync().then(function () {\n' +
    '            function loadScript(path, content) {\n' +
    '                return new Promise(function (resolve, reject) {\n' +
    '                    var s = document.createElement(\'script\');\n' +
    '                    s.src = path;\n' +
    '                    s.innerHtml = content;\n' +
    '                    s.onload = resolve;\n' +
    '                    s.onerror = reject;\n' +
    '                    document.head.appendChild(s);\n' +
    '                });\n' +
    '            }\n';

  for (const script of scripts) {
    if (script.src === 'https://connect.facebook.net/en_US/fbinstant.6.3.js') continue;
    if (script.innerHTML.length > 0) continue;
    const src = `'${script.src}'`;
    const content = `'${script.innerHTML}'`;
    windowOnloadScript.innerHTML += 'loadScript(' + src + ', ' + content + ');\n';
  }

  const gameLoader = fs.readFileSync('./facebook-build/fb-game-script-loader.js');
  const gameLoaderString = `${gameLoader}`;

  windowOnloadScript.innerHTML += gameLoaderString;

  windowOnloadScript.innerHTML += '}).then(() => FBInstant.startGameAsync())}';

  document.body.appendChild(windowOnloadScript);

  for (const script of scripts) {
    if (script.src === 'https://connect.facebook.net/en_US/fbinstant.6.3.js') continue;
    if (script.innerHTML.includes('loadScript(')) continue;
    script.parentElement.removeChild(script);
  }

  fs.writeFileSync('build/index.html', document.documentElement.outerHTML);
};
buildFacebookApp().then(r => console.log('[FacebookBuilder] Build has been created.'));